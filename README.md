# Mp3Encoder README

## UBUNTU / Linux

### Install lame library
Ubuntu: sudo apt-get install lame, sudo apt-get install libmp3lame-dev

Git clone the project

build the project from terminal
gcc -o Mp3Encoder Mp3Encoder.c -lmp3lame -pthread

run the Mp3Encoder
./Mp3Encoder

run with the folder of files to convert
./Mp3Encoder /home/parallels/files/


## Windows 10

Git clone the project

VC 2017 community edition used in this example

### Install dirent.h (1.2.1)

Download: https://web.archive.org/web/20170428133315/http://www.softagalleria.net/dirent.php

copy dirent.h --> vc include folder

### Install Pthreads (pthreads-w32-2-9-1-release)

https://sourceware.org/pthreads-win32/
Download the precompiled lib and dll.

copy pthread.h and sched.h to vc include folder

Link the pthreadVC2.lib 32 bit version during build phase

### Install LAME (3.99.5)

Download 32bit version http://www.rarewares.org/mp3-lame-libraries.php

copy lame\lame.h to vc include folder


Build the project from command line:
cl Mp3Encoder.c /link c:\prog\libmp3lame.lib c:\prog\pthreadVC2.lib

Copy the libmp3lame.dll and pthreadVC2.dll to the same folder as the executable

RUN
Mp3Encoder.exe c:\files\
