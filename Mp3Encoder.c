#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <dirent.h>
#include <string.h>
#include <lame/lame.h>
#define HAVE_STRUCT_TIMESPEC
#include <pthread.h>

// Function declarations
char* create_mp3_extension(const char *s1);
bool has_wav_extension(const char *name);
char* concat(const char *s1, const char *s2);
void *encode(void *ptr);
int files(char *dirname);

// Main starts here
int main(int argc, char *argv[] )
{
  // Check arguments - we expect a folder as input
  if (argc <2) {
    printf("\nMp3Encoder usage: Mp3Encoder +path to folder with wav-files\n");
    printf("Example: ./Mp3Encoder /home/user/files\n");
    printf("Example windows: Mp3Encoder.exe C:\\files\\\n");
    return 0;
  }

  // Check how many files to encode
  int ret = files(argv[1]);
  //printf("Amount of files %d \n", ret);

  // Start the main loop
  // Check if second argument is valid folder
  DIR *dir;
  struct dirent *cursor;
  if ((dir = opendir (argv[1])) != NULL) {
    // We have a valid folder take all wav files and encode them to mp3
    // init thread id variable and return variable
    //pthread_t tids[ret]; // Set the number of threads to the number of files to be processed
    pthread_t* tids = malloc(ret * sizeof(pthread_t));
    int threadid=0;
    int iret = 0;

    // read through the dir
    printf("Processing (%d) files...\n", ret);
    while ((cursor = readdir (dir)) != NULL) {
      // only accept files not having "..", "."" and only with .wav extension
      if (cursor->d_name[0] != '.' && cursor->d_name[strlen(cursor->d_name)-1] != '~') {
        if (has_wav_extension( (char*) &cursor->d_name)) {
          char* result = concat(argv[1], cursor->d_name);

          iret = pthread_create(&tids[threadid], NULL, encode, (void*) result);
          if(iret)
          {
              fprintf(stderr,"Error - pthread_create() return code: %d\n",iret);
              exit(EXIT_FAILURE);
          }
          threadid++;
          result=NULL;
        }
      }
    }
    // wait until threads have done their work
    for (int i=0;i<threadid;i++) {
      pthread_join(tids[i], NULL);
    }
    closedir (dir);
    free(tids);
    printf("\n");
  } else {
    printf("Mp3Encoder - Error: the folder %s does not exist\n", argv[1]);
    return 0;
  }
}
// End main -----------------------------------------------------------

// Encode one wav file with standard settings
void *encode(void *ptr) {

  // encode one file
  int read, write;

  char *filename;
  filename = (char *) ptr;
  FILE *pcm = fopen(filename, "rb");
  FILE *mp3 = fopen(create_mp3_extension(filename), "wb");

  const int PCM_SIZE = 8192;
  const int MP3_SIZE = 8192;

  short int* pcm_buffer = malloc((PCM_SIZE*2) * sizeof(short int));

  if (pcm_buffer == NULL){
      printf("Mp3Encoder - Error: can't reserve memory for pcm_buffer");
      return 0;
  }

  unsigned char* mp3_buffer = malloc(MP3_SIZE * sizeof(unsigned char));

  if (mp3_buffer == NULL){
    printf("Mp3Encoder - Error: can't reserve memory for mp3_buffer");
    return 0;
  }


  // Init and set quality
  lame_t lame = lame_init();
  lame_set_in_samplerate(lame, 44100);
  lame_set_VBR(lame, vbr_default);
  lame_init_params(lame);

  // Loop through the file and encode
  do {
      read = fread(pcm_buffer, 2*sizeof(short int), PCM_SIZE, pcm);
      if (read == 0) {
          write = lame_encode_flush(lame, mp3_buffer, MP3_SIZE);
      }
      else {
          write = lame_encode_buffer_interleaved(lame, pcm_buffer, read, mp3_buffer, MP3_SIZE);
          fwrite(mp3_buffer, write, 1, mp3);
      }
  } while (read != 0);

  // Cleanup
  free(pcm_buffer);
  free(mp3_buffer);
  lame_close(lame);
  fclose(mp3);
  fclose(pcm);
  pthread_exit(0);
}

// check for wav extension returns 1 (true) if file extension is wav
bool has_wav_extension(const char *name)
{
    size_t len = strlen(name);
    return len > 4 && strcmp(name + len - 4, ".wav") == 0;
}

// Concat 2 strings
char* concat(const char *s1, const char *s2)
{
    const size_t len1 = strlen(s1);
    const size_t len2 = strlen(s2);
    char *result = malloc(len1+len2+1);//+1 for the null-terminator
    memcpy(result, s1, len1);
    memcpy(result+len1, s2, len2+1);//+1 to copy the null-terminator
    return result;
}

// Replace wav with mp3 and return the full filname with path
char* create_mp3_extension(const char *s1)
{
    char *ending = "mp3";
    const size_t len1 = strlen(s1);
    const size_t len2 = strlen(ending);
    char *result = malloc(len1);

    // copy the inital string minus the extension wav (3 chars)
    memcpy(result, s1, len1-3);
    memcpy(result+len1-3, ending, len2+1);

    return result;
}

// Return count of files to process
int files(char *dirname)
{
  DIR *dir;
  struct dirent *cursor;
  int numfiles=0;

  if ((dir = opendir (dirname)) != NULL) {
    while ((cursor = readdir (dir)) != NULL) {
      // only accept files not having "..", "."" and only with .wav extension
      if (cursor->d_name[0] != '.' && cursor->d_name[strlen(cursor->d_name)-1] != '~') {
        if (has_wav_extension( (char*) &cursor->d_name)) {
          numfiles++;
        }
      }
    }
  }
  closedir (dir);
  return numfiles;
}
